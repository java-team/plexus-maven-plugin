#!/bin/sh -e

VERSION=$2
TAR=../plexus-maven-plugin_$VERSION.orig.tar.gz
DIR=plexus-maven-plugin-$VERSION
TAG=$(echo "plexus-maven-plugin-$VERSION" | sed -re's/~(alpha|beta)/-\1-/')

svn export http://svn.codehaus.org/plexus/plexus-maven-plugin/tags/${TAG}/ $DIR
GZIP=--best tar -c -z -f $TAR --exclude '*.jar' --exclude '*.class' $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir && echo "moved $TAR to $origDir"
fi
